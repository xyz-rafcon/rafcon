Introduction
============
We fork the repo from https://github.com/DLR-RM/RAFCON, and 
add some features to fit our need. This features include properties data, 
bézier curve of transition line, set break point in source code...

All new features are in CHANGELOG.md.

How to create .deb pacakge  
===========================

.. code-block:: bash

   python setup.py --command-packages=stdeb.command sdist_dsc
   VERSION=$(grep -oP -m 1 '(?<=## ).*(?=\()' CHANGELOG.md)
   BUILD_PACKAGE_NAME=${PACKAGE_NAME}_$VERSION-1_all
   cd deb_dist
   cd */.
   DEB_BUILD_OPTIONS=nocheck dpkg-buildpackage -rfakeroot -uc -us

How to launch from source code
===============================

.. code-block:: bash

    # Clone rafcon in any directory. Recomment to clone into ~/
    git clone https://xyz-rafcon@bitbucket.org/xyz-rafcon/rafcon.git

    # add these lines into ~/.bashrc.

    export PYTHONPATH=/some/personal/path/rafcon/source:$PYTHONPATH
    export PATH=/some/personal/path/rafcon/bin:$PATH

Restart the terminal.
Run 'rafcon' 
You should see the rafcon window pop out. There might be some visual error such as white window frame and inputs.

