# rafcon 

## 1.3.1(2022.2.24)
1. remove gvm's access_key to never use time-consuming function global_variable_id_generator.

## 1.3.0(2022.2.16)
1. we remove @@ at the end of the execution state name(self._name) and add "*" in gui.

## 1.2.17(2022.2.17)
1. show state execution time and transition time

## 1.2.16(2022.1.25)
1. default close rviz

## 1.2.15(2021.12.29)
1. fix 16.04 not found bug
2. cpickle bug
3. fix bugs of cannot update code in 20.04


## 1.2.14(2021.12.28)
1. fix 20.04 matplotlib backend crash

## 1.2.13(2021.12.16)
1. fix bugs when barrier concurrency state cannot find transition.

## 1.2.12(2021.12.16)
1. fix bugs when barrier concurrency state cannot find transition.

## 1.2.11(2021.12.13)
1. snap debugger terminal to left side.
2. clear breakpoints after editing the script.

## 1.2.10(2021.12.1)
1. fix bugs when copy state machine.

## 1.2.9(2021.12.1)
1. clear line marks after open external button is clicked
2. update_all refactoring.
3. add check with libraries.
4. add globally upgrade from libraries.
5. add checking the state's code same as library's. If not, the state's name will add "@@".
6. add backup current state machine when update.
7. fix bugs when INPUT_DATA becomes STATIC after ctrl+z
8. fix bugs of missing data_source_translation when dragging a new state from library.
9. add IGNORED_UPGRADE_STATE_NAME to config.yaml to ignore the state we don't want to update because like constant.

## 1.2.8(2021.11.30)
1. support for breakpoints in rafcon source editor

## 1.2.7(2021.11.25)
1. add palltieze templates 

## 1.2.6(2021.11.12)
1. speed up 

## 1.2.5(2021.11.10)
1. show version of the state in rafcon-lib
2. add Diff library button to diff the source code in rafcon-lib between current state

## 1.2.4(2021.11.10)
1. fix bug of opening properties data external will delete state's code
2. fix bug of update properties data from library to current state will not update data_source_translation

## 1.2.3(2021.11.9)
1. add print data port's data when choosing data port on GUI

## 1.2.2(2021.11.8)
1. add chinese tooltip for properties data

## 1.2.1(2021.11.5)
1. fix takes too much memory when running

## 1.2.0(2021.10.19)
1. fix bugs of update properties data when its type is INPUT_PORT

## 1.1.6(2021.10.12)
1. fix bezier curve bugs

## 1.1.5(2021.8.13)
1. add close and is_running in rafcon launcher

## 1.1.4(2021.8.10)
1. fix bugs when update outcomes from library

## 1.1.3(2021.8.2)
1. fix bugs when update properties_data from library

## 1.1.2(2021.7.28)
1. fix bugs when update properties_data
2. test 

## 1.1.1(2021.7.19)
1. embed rviz 
2. test package

## 1.1.0(2021.6.1)
1. add properties data type
2. add support for choose_optional_data

## 1.0.9(2021.7.1)
1. clear gvm also in step mode
2. define env-manager's error

## 1.0.8(2021.6.1)
1. fixed bugs in python3

## 1.0.7(2021.5.28)
1. add changelog and compatible with rafcon-lib(not add docs in rafcon-lib)

## 1.0.6(2021.5.28)
1. update all of a state
2. fix bug of crash

