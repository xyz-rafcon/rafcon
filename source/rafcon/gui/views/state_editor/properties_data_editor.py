from os import stat
from gi.repository import GObject
from gi.repository import Gtk
from rafcon.gui import glade
from gtkmvc3.view import View
import rafcon.gui.helpers.label as gui_helper_label

from rafcon.gui.utils import constants

from rafcon.utils import log
logger = log.get_logger(__name__)


class PropertiesDataEditorView(View):

    """ The view of the Properties Data Editor widget

    """

    builder = glade.get_glade_path("properties_data_editor.glade")
    top = 'properties_data_vbox'
    KEY_STORAGE_ID = 0
    VALUE_STORAGE_ID = 1
    IS_DICT_STORAGE_ID = 2
    VIEW_DATA_TYPE_ID = 3
    DATA_SOURCE_TYPE_ID = 4

    KEY_COLUMN_ID = 0
    VALUE_COLUMN_ID = 1
    IS_DICT_COLUMN_ID = 2
    DATA_TYPE_COLUMN_ID = 3
    DATA_SOURCE_COLUMN_ID = 4
    def __init__(self):
        super(PropertiesDataEditorView, self).__init__()
        self.scrollbar_widget = self['properties_data_scroller']
        self['delete_entry'].set_border_width(constants.BUTTON_BORDER_WIDTH)
        self['new_dict_entry'].set_border_width(constants.BUTTON_BORDER_WIDTH)
        self['new_entry'].set_border_width(constants.BUTTON_BORDER_WIDTH)
        tree_view = self["properties_data_tree_view"]
        tree_view.set_property('has-tooltip', True)

        # prepare tree view columns
        key_renderer = Gtk.CellRendererText()
        key_renderer.set_property('editable', True)
        col = Gtk.TreeViewColumn('Key', key_renderer, text=self.KEY_STORAGE_ID)
        tree_view.append_column(col)

        value_renderer = Gtk.CellRendererText()
        value_renderer.set_property('editable', True)
        col = Gtk.TreeViewColumn('Value', value_renderer, text=self.VALUE_STORAGE_ID)
        tree_view.append_column(col)

        is_dict_renderer = Gtk.CellRendererText()
        col = Gtk.TreeViewColumn('Is Dict', is_dict_renderer, text=self.IS_DICT_STORAGE_ID)
        tree_view.append_column(col)

        data_type_renderer = Gtk.CellRendererText()
        data_type_renderer.set_property('editable', True)
        col = Gtk.TreeViewColumn('Data type', data_type_renderer, text=self.VIEW_DATA_TYPE_ID)
        tree_view.append_column(col)

        data_source = Gtk.ListStore(GObject.TYPE_STRING)
        source_origins = ["STATIC", "INPUT_PORT"]
        for item in source_origins:
            data_source.append([item])
        
        self.renderer_combo = Gtk.CellRendererCombo()
        self.renderer_combo.set_property("editable", True)
        self.renderer_combo.set_property("model", data_source)
        self.renderer_combo.set_property("text-column", 0)
        self.renderer_combo.set_property("has-entry", False)

        column_combo = Gtk.TreeViewColumn("Data Source", self.renderer_combo, text=self.DATA_SOURCE_TYPE_ID)
        tree_view.append_column(column_combo)

        logger.debug("[PropertiesDataEditorView] add new renderer_combo")

        gui_helper_label.ellipsize_labels_recursively(self['properties_data_toolbar'])

    def query_tooltip(self, treeview, x, y, keyboard_mode, tooltip, state):
        model, paths = treeview.get_selection().get_selected_rows()
        
        if not paths:
            return False

        if len(paths) == 1:
            paths = paths[0]


        path_split = str(paths).split(":")


        def sum_string(index):
            i = 0
            return_value = []
            while i <= index:
                return_value.append(path_split[i])
                i += 1

            s = ":"
            
            path = s.join(return_value)

            return self["properties_data_tree_view"].get_model()[path][0]

        input_name = "" 
        for index in range(len(path_split)):
            input_name += "/"
            input_name += sum_string(index)

        if not input_name in state.data_source_translation:
            logger.debug("Key: {} is not in data_source_translation".format(input_name))
            return False
        
        if not state.data_source_translation[input_name]:
            logger.debug("Key: {} in data_source_translation is empty".format(input_name))
            return False

        tooltip.set_text(state.data_source_translation[input_name])
        
        return True

    def on_type_combo_changed(self, widget, path, text, state):
        if state:
            print(path)

        path_split = path.split(":")

        path_list = []

        def sum_string(index):
            i = 0
            return_value = []
            while i <= index:
                return_value.append(path_split[i])
                i += 1

            s = ":"
            
            path = s.join(return_value)

            return self["properties_data_tree_view"].get_model()[path][0]

        for index in range(len(path_split)):
            path_list.append(sum_string(index))

        type_json_tag = state.get_source_data_type(path_list)
        current_type = type_json_tag["current_type"]
        optional_types = type_json_tag["optional_types"]


        def get_input_state_name_dict(state):
            input_state_name_dict = {}
            for data_port_id, value in state.input_data_ports.items():
                input_state_name_dict[value.name] = data_port_id

            path_ = path.split(":")
            path_list = [path_[0]]
            for i in range(1, len(path_)):
                path_list.append(path_list[-1] + ":" + path_[i])

            input_name = "" 
            for i in range(len(path_list)):
                input_name += "/"
                input_name += self["properties_data_tree_view"].get_model()[path_list[i]][0]

            return input_state_name_dict, input_name

        if text not in optional_types:
            logger.warning("{} is not in optional_types: {}".format(text, optional_types))
        else:
            state.change_source_data_type(path_list, str(text))
            old_type = self["properties_data_tree_view"].get_model()[path][self.DATA_SOURCE_TYPE_ID]
            self["properties_data_tree_view"].get_model()[path][self.DATA_SOURCE_TYPE_ID] = str(text)

            key = self["properties_data_tree_view"].get_model()[path][0]
            value = self["properties_data_tree_view"].get_model()[path][1]
            is_dict = self["properties_data_tree_view"].get_model()[path][2]
            data_type = self["properties_data_tree_view"].get_model()[path][3]

            if is_dict:
                data_type = dict

            if str(old_type) == "STATIC" and str(text) == "INPUT_PORT":
                input_state_name_dict, input_name = get_input_state_name_dict(state)
                if input_name in input_state_name_dict:
                    logger.error("Input name: {} is already existed: {}".format(input_name))
                else:
                    state.add_input_data_port(input_name, data_type)

                state.check_properties_data_input_sanity()

            elif str(old_type) == "INPUT_PORT" and str(text) == "STATIC":
                input_state_name_dict, input_name = get_input_state_name_dict(state)
                if input_name in input_state_name_dict:
                    state.remove_input_data_port(input_state_name_dict[input_name])
                else:
                    logger.error("Input port name: {} does not existed".format(input_name))

                state.check_properties_data_input_sanity()

            elif str(old_type) == "INPUT_PORT" and str(text) == "GLOBAL":
                input_state_name_dict = {}
                for data_port_id, value in state.input_data_ports.items():
                    input_state_name_dict[value.name] = data_port_id

                input_name = self["properties_data_tree_view"].get_model()[path][0]

                if input_name in input_state_name_dict:
                    state.remove_input_data_port(input_state_name_dict[input_name])
                else:
                    logger.error("Input port name: {} does not existed".format(input_name))
            elif str(old_type) == "GLOBAL" and str(text) == "INPUT_PORT":
                input_state_name_dict = {}
                for data_port_id, value in state.input_data_ports.items():
                    input_state_name_dict[value.name] = data_port_id

                input_name = self["properties_data_tree_view"].get_model()[path][0]

                if input_name in input_state_name_dict:
                    logger.error("Input name: {} is already existed: {}".format(input_name))
                else:
                    state.add_input_data_port(input_name, data_type)

