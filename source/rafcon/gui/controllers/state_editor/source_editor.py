# Copyright (C) 2015-2018 DLR
#
# All rights reserved. This program and the accompanying materials are made
# available under the terms of the Eclipse Public License v1.0 which
# accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
#
# Contributors:
# Annika Wollschlaeger <annika.wollschlaeger@dlr.de>
# Franz Steinmetz <franz.steinmetz@dlr.de>
# Lukas Becker <lukas.becker@dlr.de>
# Rico Belder <rico.belder@dlr.de>
# Sebastian Brunner <sebastian.brunner@dlr.de>

"""
.. module:: source_editor
   :synopsis: A module holds the controller to edit the state source script text.

"""

import imp
from future import standard_library
standard_library.install_aliases()
import os
from gi.repository import Gtk
import contextlib
from pylint import lint
try:
    from pylint.reporters.json import JSONReporter
except ModuleNotFoundError:
    from pylint.reporters.json_reporter import JSONReporter
from io import StringIO
from astroid import MANAGER
from pkg_resources import resource_filename

import rafcon
from rafcon.core.states.execution_state import ExecutionState
from rafcon.core.states.library_state import LibraryState
from rafcon.core.storage import storage
from rafcon.core.storage.storage import SCRIPT_FILE

from rafcon.gui.controllers.utils.editor import EditorController
from rafcon.gui.singleton import state_machine_manager_model
from rafcon.gui.config import global_gui_config
from rafcon.core.config import global_config
from rafcon.gui.utils.dialog import RAFCONButtonDialog, UpdateSourceDialog, ShowCodeDiffDialog
from rafcon.gui.models import AbstractStateModel, LibraryStateModel
from rafcon.gui.views.state_editor.source_editor import SourceEditorView
from rafcon.gui.utils.external_editor import AbstractExternalEditor

from rafcon.utils import filesystem
from rafcon.utils.constants import RAFCON_TEMP_PATH_STORAGE
from rafcon.utils import log
from rafcon.utils.helper_function import update_all_same_name_script_recursively, find_root_state_recursively
from rafcon.utils.helper_function import update_all_same_name_properties_data_recursively
from rafcon.utils.helper_function import update_all_same_name_port_data_recursively
from rafcon.utils.helper_function import update_all_same_name_outcome_recursively
from rafcon.utils.helper_function import get_xyz_lib_path, get_relative_path_of_lib, backup_old_state_machine

logger = log.get_logger(__name__)


class SourceEditorController(EditorController, AbstractExternalEditor):
    """Controller handling the source editor in Execution States.

    :param
    :param rafcon.gui.views.source_editor.SourceEditorView view: The GTK view showing the source editor.
    """
    # TODO Missing functions
    # - Code function-expander
    # - Code completion

    tmp_file = os.path.join(RAFCON_TEMP_PATH_STORAGE, 'file_to_get_pylinted.py')

    def __init__(self, model, view):
        assert isinstance(model, AbstractStateModel)
        assert isinstance(view, SourceEditorView)
        lib_with_show_content = isinstance(model, LibraryStateModel) and not model.show_content()
        model = model.state_copy if lib_with_show_content else model

        # unfortunately this cannot be down with super, as gtkmvc3 does not use super() consistently
        EditorController.__init__(self, model, view, observed_method="script_text")
        AbstractExternalEditor.__init__(self)
        self.saved_initial = False

    def register_view(self, view):
        super(SourceEditorController, self).register_view(view)

        view["update_all_button"].connect('clicked', self.update_all_clicked)
        view["code_diff_button"].connect('clicked', self.code_diff_button_clicked)
        view['open_external_button'].connect('clicked', self.open_externally_clicked_with_marks_clear)
        view['apply_button'].connect('clicked', self.apply_clicked)
        view['cancel_button'].connect('clicked', self.cancel_clicked)
        view.textview.connect("line-mark-activated", self.line_mark_activated, None)
        if not hasattr(self.model.state, "breakpoints"):
            self.model.state.breakpoints = set()

        if self.model.state.version:
            view['version_label'].set_text("v"+self.model.state.version)

        view['pylint_check_button'].set_active(global_gui_config.get_config_value('CHECK_PYTHON_FILES_WITH_PYLINT', False))

        view['pylint_check_button'].set_tooltip_text("Change global default value in GUI config.")
        view['apply_button'].set_tooltip_text(global_gui_config.get_config_value('SHORTCUTS')['apply'][0])
        view['open_external_button'].set_tooltip_text("Open source in external editor. " +
                                                      global_gui_config.get_config_value('SHORTCUTS')['open_external_editor'][0])

        if isinstance(self.model.state, LibraryState) or self.model.state.get_next_upper_library_root_state():
            view['pylint_check_button'].set_sensitive(False)
            view.textview.set_editable(False)
            view['apply_button'].set_sensitive(False)
            view['cancel_button'].set_sensitive(False)
            view['open_external_button'].set_sensitive(False)
            view['update_all_button'].set_sensitive(False)

    @property
    def source_text(self):
        return self.model.state.script_text

    @source_text.setter
    def source_text(self, text):
        try:
            self.model.state.script_text = text
        except Exception as e:
            logger.error("The script was saved, but cannot be compiled: {}: {}".format(e.__class__.__name__, e))

    # ==================================== External Editor functions start ====================================

    def get_file_name(self):
        """ Implements the abstract method of the ExternalEditor class.
        """
        return storage.SCRIPT_FILE

    def save_file_data(self, path):
        """ Implements the abstract method of the ExternalEditor class.
        """
        sm = self.model.state.get_state_machine()
        if sm.marked_dirty and not self.saved_initial:
            try:
                # Save the file before opening it to update the applied changes. Use option create_full_path=True
                # to assure that temporary state_machines' script files are saved to
                # (their path doesnt exist when not saved)
                filesystem.write_file(os.path.join(path, storage.SCRIPT_FILE),
                                      self.view.get_text(), create_full_path=True)
            except IOError as e:
                # Only happens if the file doesnt exist yet and would be written to the temp folder.
                # The method write_file doesnt create the path
                logger.error('The operating system raised an error: {}'.format(e))
            self.saved_initial = True

    def set_editor_lock(self, lock):
        """ Implements the abstract method of the ExternalEditor class.
        """
        self.view.set_enabled(not lock)

    def load_and_set_file_content(self, file_system_path):
        """ Implements the abstract method of the ExternalEditor class.
        """
        content = filesystem.read_file(file_system_path, storage.SCRIPT_FILE)
        if content is not None:
            self.set_script_text(content)

    # ==================================== External Editor functions end ====================================

    def update_all_same_state(self, update_mode, dialog):
        ## We update code
        if update_mode == "UPDATE_FROM_THIS":
            # update all state in state machine
            root_state = find_root_state_recursively(self.model.state)

            backup_old_state_machine(root_state.parent.file_system_path)

            ignore_modify = dialog.ignore_modified_name_check_box.get_active()

            update_all_same_name_script_recursively(root_state, self.model.state, ignore_modify)
            update_all_same_name_properties_data_recursively(root_state, self.model.state, ignore_modify)
            update_all_same_name_port_data_recursively(root_state, self.model.state, ignore_modify)
            update_all_same_name_outcome_recursively(root_state, self.model.state, ignore_modify)
            

            lib_path = get_xyz_lib_path()
            full_path = os.path.join(lib_path, self.model.state.source_file_path)
            base_name = os.path.basename(full_path)
            folder_path = str(full_path.split(base_name)[0])

            source_lib_need_update = dialog.update_code_check_box.get_active()
            if source_lib_need_update:
                if os.path.exists(full_path):
                    filesystem.write_file(full_path, self.model.state.script_text)
                else:
                    logger.error("Script file could not be opened or was empty: {}".format(full_path))

            lib_properties_data_need_update = dialog.update_properties_data_check_box.get_active()
            if lib_properties_data_need_update:
                if os.path.exists(folder_path):
                    from rafcon.core.storage.storage import save_properties_data_for_state
                    save_properties_data_for_state(self.model.state, folder_path)
                else:
                    logger.error("{} file could not be opened or was empty: {}".format(PROPERTIES_DATA_FILE, folder_path))

            lib_data_port_need_update = dialog.update_data_port_check_box.get_active()
            if lib_data_port_need_update:
                if os.path.exists(folder_path):
                    from rafcon.core.storage.storage import FILE_NAME_CORE_DATA
                    from rafcon.utils import storage_utils
                    storage_utils.write_dict_to_json(self.model.state, os.path.join(folder_path, FILE_NAME_CORE_DATA))
                else:
                    logger.error("{} file could not be opened or was empty: {}".format(FILE_NAME_CORE_DATA, folder_path))

            lib_data_port_need_update = dialog.update_data_port_check_box.get_active()
            if lib_data_port_need_update:
                if os.path.exists(folder_path):
                    from rafcon.core.storage.storage import FILE_NAME_CORE_DATA
                    from rafcon.utils import storage_utils
                    storage_utils.write_dict_to_json(self.model.state, os.path.join(folder_path, FILE_NAME_CORE_DATA))
                else:
                    logger.error("{} file could not be opened or was empty: {}".format(FILE_NAME_CORE_DATA, folder_path))

        else: ## UPDATE_FROM_SOURCE
            ignore_modify = dialog.ignore_modified_name_check_box.get_active()

            lib_path = get_xyz_lib_path()
            source_file_path = get_relative_path_of_lib(self.model.state.name)
            if self.model.state.source_file_path == '' and (not source_file_path):
                logger.error("Cannot find the source_file_path of state: {}".format(self.model.state.name))
                return

            if self.model.state.source_file_path == '' and source_file_path:
                self.model.state.source_file_path = source_file_path
                logger.warning("The state's source_file_path is empty, we find the same named state in library and set it to this state")

            template_state_name = self.model.state.source_file_path.split("/")[-3]
            this_state_name = self.model.state.name

            if template_state_name != this_state_name:
                logger.error("source_file_path's state name: {} is different from state's name: {}".format(
                    self.model.state.source_file_path.split("/")[-3],
                    self.model.state.name))
                return

            full_path = os.path.join(lib_path, self.model.state.source_file_path)

            if os.path.exists(full_path):

                from rafcon.core.storage.storage import FILE_NAME_CORE_DATA, load_data_file
                from rafcon.utils import storage_utils
                from rafcon.core.storage.storage import load_data_file, PROPERTIES_DATA_FILE
                base_name = os.path.basename(full_path)
                folder_path = str(full_path.split(base_name)[0])
                properties_data = load_data_file(os.path.join(folder_path, PROPERTIES_DATA_FILE), True)
                path_core_data = os.path.join(folder_path, FILE_NAME_CORE_DATA)
                templates_state = load_data_file(path_core_data)

                ## using library's code
                file_content = filesystem.read_file(full_path)
                templates_state.script_text = file_content

                ## using library's propeties_data
                templates_state.properties_data = properties_data

                root_state = find_root_state_recursively(self.model.state)
                backup_old_state_machine(root_state.parent.file_system_path)
                update_all_same_name_script_recursively(root_state, templates_state, ignore_modify)
                update_all_same_name_port_data_recursively(root_state, templates_state, ignore_modify)
                update_all_same_name_properties_data_recursively(root_state, templates_state, ignore_modify)
                update_all_same_name_outcome_recursively(root_state, templates_state, ignore_modify)
            else:
                logger.error("Script file could not be opened or was empty: {}".format(full_path))

    def code_diff_button_clicked(self, button):
        self.open_externally_clicked(button, diff = True)
    
    def open_externally_clicked_with_marks_clear(self, button):
        self.clear_line_marks()
        self.open_externally_clicked(button)

    def line_mark_activated(self, gutter, place, ev, user_data):
        text_buffer = self.view.get_buffer()
        if ev.button.button == 1:
            mark_list = text_buffer.get_source_marks_at_line(place.get_line(), self.view.MARK_TYPE_BP)
            if mark_list:
                text_buffer.delete_mark(mark_list[0])
                self.model.state.breakpoints.discard(place.get_line())
            else:
                text_buffer.create_source_mark(None, self.view.MARK_TYPE_BP, place)
                self.model.state.breakpoints.add(place.get_line())

    def extract_line_marks(self):
        '''
            Extract all line marks from textview to execution state model
        '''
        text_buffer = self.view.get_buffer()

        self.model.state.breakpoints = set()
        # The iter does not work, gtk keeps complaining that iter is invalid
        # I use this silly line by line method to clear all breakpoints
        for line_idx in range(text_buffer.get_line_count()):
            mark_list = text_buffer.get_source_marks_at_line(line_idx, self.view.MARK_TYPE_BP)
            if mark_list:
                self.model.state.breakpoints.add(line_idx)

    def clear_line_marks(self):
        text_buffer = self.view.get_buffer()

        self.model.state.breakpoints = set()
        # The iter does not work, gtk keeps complaining that iter is invalid
        # I use this silly line by line method to clear all breakpoints
        for line_idx in range(text_buffer.get_line_count()):
            mark_list = text_buffer.get_source_marks_at_line(line_idx, self.view.MARK_TYPE_BP)
            for mark in mark_list:
                text_buffer.delete_mark(mark)

    def rebuild_line_marks(self):
        text_buffer = self.view.get_buffer()
        self.clear_line_marks()
        for line in self.model.state.breakpoints:
            mark_list = text_buffer.get_source_marks_at_line(line, self.view.MARK_TYPE_BP)
            if mark_list:
                continue
            else:
                place = text_buffer.get_iter_at_line(line)
                text_buffer.create_source_mark(None, self.view.MARK_TYPE_BP, place)

    def update_all_clicked(self, button):
        if not isinstance(self.model.state, ExecutionState):
            return

        markup_text = "This will help you to update source code of execution state"

        lib_path = get_xyz_lib_path()
        
        source_file_path_full = os.path.join(lib_path, self.model.state.source_file_path)

        # create a new RAFCONButtonInputDialog, add a checkbox and add the text 'remember' to it
        dialog = UpdateSourceDialog(markup_text,
                                    button_texts=("Apply", "Cancel"),
                                    width=800,
                                    height=200,
                                    standalone=False,
                                    state_name=self.model.state.name, 
                                    state_absolute_path = source_file_path_full)

        dialog.show_all()
        response_id = dialog.run()
        if response_id == 1:  # Apply pressed
            if self.model.state.source_file_path == '':
                source_file_path = get_relative_path_of_lib(self.model.state.name)
                if not source_file_path:

                    if self.model.state.name == "Decider":
                        from rafcon.core.script import BARRIER_DEFAULT_SCRIPT
                        self.model.state.script_text = BARRIER_DEFAULT_SCRIPT
                    else:
                        logger.error("Cannot find the source_file_path of state: {}".format(self.model.state.name))
                    dialog.destroy()
                    return

                self.model.state.source_file_path = source_file_path
                logger.warning("The state's source_file_path is empty, we find the same named state in library and set it to this state")
            try:
                if dialog.update_code_from_this_button.get_active():
                    update_mode = "UPDATE_FROM_THIS"
                else:
                    update_mode = "UPDATE_FROM_SOURCE"
                self.update_all_same_state(update_mode, dialog)
            except Exception as e:
                logger.error(e)
                logger.error("This is a bug, please fix this as soon as possible!. Please check this state's source_file_path is valid.")

        elif response_id in [2, -4]:  # Cancel or Close pressed
            pass
        else:
            raise ValueError("Response id: {} is not considered".format(response_id))
        dialog.destroy()

    def apply_clicked(self, button):
        """Triggered when the Apply button in the source editor is clicked.

        """
        if isinstance(self.model.state, LibraryState):
            logger.warning("It is not allowed to modify libraries.")
            self.view.set_text("")
            return

        # Ugly workaround to give user at least some feedback about the parser
        # Without the loop, this function would block the GTK main loop and the log message would appear after the
        # function has finished
        # TODO: run parser in separate thread
        while Gtk.events_pending():
            Gtk.main_iteration_do(False)

        # get script
        current_text = self.view.get_text()

        # Directly apply script if linter was deactivated
        if not self.view['pylint_check_button'].get_active():
            self.extract_line_marks()
            self.set_script_text(current_text)
            self.rebuild_line_marks()
            return
        elif self.model.state.breakpoints:
            logger.error("Parsing script with breakpoints is not suppored currently!")
            return

        logger.debug("Parsing execute script...")
        with open(self.tmp_file, "w") as text_file:
            text_file.write(current_text)

        # clear astroid module cache, see http://stackoverflow.com/questions/22241435/pylint-discard-cached-file-state
        MANAGER.astroid_cache.clear()
        lint_config_file = resource_filename(rafcon.__name__, "pylintrc")
        args = ["--rcfile={}".format(lint_config_file)]  # put your own here
        with contextlib.closing(StringIO()) as dummy_buffer:
            json_report = JSONReporter(dummy_buffer.getvalue())
            try:
                lint.Run([self.tmp_file] + args, reporter=json_report, exit=False)
            except:
                logger.exception("Could not run linter to check script")
        os.remove(self.tmp_file)

        if json_report.messages:
            def on_message_dialog_response_signal(widget, response_id):
                if response_id == 1:
                    self.set_script_text(current_text)
                else:
                    logger.debug("The script was not saved")
                widget.destroy()

            message_string = "Are you sure that you want to save this file?\n\nThe following errors were found:"

            line = None
            for message in json_report.messages:
                (error_string, line) = self.format_error_string(message)
                message_string += "\n\n" + error_string

            # focus line of error
            if line:
                tbuffer = self.view.get_buffer()
                start_iter = tbuffer.get_start_iter()
                start_iter.set_line(int(line)-1)
                tbuffer.place_cursor(start_iter)
                message_string += "\n\nThe line was focused in the source editor."
                self.view.scroll_to_cursor_onscreen()

            # select state to show source editor
            sm_m = state_machine_manager_model.get_state_machine_model(self.model)
            if sm_m.selection.get_selected_state() is not self.model:
                sm_m.selection.set(self.model)

            dialog = RAFCONButtonDialog(message_string, ["Save with errors", "Do not save"],
                               on_message_dialog_response_signal,
                               message_type=Gtk.MessageType.WARNING, parent=self.get_root_window())
            result = dialog.run()
        else:
            self.set_script_text(current_text)

    @staticmethod
    def format_error_string(message):
        return "Line {}: {} ({})".format(message["line"], message["message"], message["symbol"]), message["line"]
