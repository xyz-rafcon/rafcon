import shlex
import subprocess

from rafcon.utils import log
_logger = log.get_logger(__name__)


def execute_command_with_path_in_process(command, path, shell=False, env=None, cwd=None, logger=None):
    """Executes a specific command in a separate process with a path as argument.

    :param command: the command to be executed
    :param path: the path as first argument to the shell command
    :param bool shell: Whether to use a shell
    :param str cwd: The working directory of the command
    :param logger: optional logger instance which can be handed from other module
    :return: None
    """
    if logger is None:
        logger = _logger
    logger.debug("Opening path with command: {0} {1}".format(command, path))
    # This splits the command in a matter so that the command gets called in a separate shell and thus
    # does not lock the window.
    args = shlex.split('{0} "{1}"'.format(command, path))
    try:
        subprocess.Popen(args, shell=shell, cwd=cwd, env=env)
        return True
    except OSError as e:
        logger.error('The operating system raised an error: {}'.format(e))
    return False


def execute_command_in_process(command, shell=False, cwd=None, env=None, executable='/bin/bash', logger=None):
    """ Executes a specific command in a separate process

    :param command: the command to be executed
    :param bool shell: Whether to use a shell
    :param str cwd: The working directory of the command
    :param logger: optional logger instance which can be handed from other module
    :return: None
    """
    if logger is None:
        logger = _logger
    logger.debug("Run shell command: {0}".format(command))
    try:
        subprocess.Popen(command, shell=shell, cwd=cwd, env=env, executable=executable)
        return True
    except OSError as e:
        logger.error('The operating system raised an error: {}'.format(e))
    return False


import psutil
## https://thispointer.com/python-check-if-a-process-is-running-by-name-and-find-its-process-id-pid/
def check_if_process_running(processName):
    '''
    Check if there is any running process that contains the given name processName.
    '''
    #Iterate over the all the running process
    for proc in psutil.process_iter():
        try:
            # Check if process name contains the given name string.
            if processName.lower() in proc.name().lower():
                return True
        except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
            pass
    return False
