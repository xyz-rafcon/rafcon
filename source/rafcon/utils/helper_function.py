
#!/usr/bin/env python
import imp
import os
import shutil

import ipdb
try:
    from StringIO import StringIO ## for Python 2
except ImportError:
    from io import StringIO ## for Python 3

import tokenize
import re
from yapf.yapflib.yapf_api import FormatCode
from rafcon.core.states.container_state import ContainerState
from rafcon.core.state_machine import StateMachine
from rafcon.core.states.concurrency_state import ConcurrencyState
import rafcon.core.config as config
from rafcon.core.library_manager import LibraryManager
from rafcon.utils import log

logger = log.get_logger(__name__)

def get_modified_name(state):
    name_text = state.name
    return name_text + "*" if state.different_from_lib else name_text

def find_target_state_from_container_recursively(container_state, state_id, parent_id):
    if state_id in container_state.states.keys() and container_state.state_id == parent_id:
        return container_state.states[state_id]
    else:
        for state in container_state.states.values():
            if isinstance(state, ContainerState):
                target_state = find_target_state_from_container_recursively(state, state_id, parent_id)
                if target_state:
                    return target_state
        return None


def find_root_state_recursively(state):
    if isinstance(state.parent, StateMachine):
        return state
    else:
        return find_root_state_recursively(state.parent)

def find_state_by_id_globally(search_start_state, state_id, parent_id):
    root_state = find_root_state_recursively(search_start_state)
    return find_target_state_from_container_recursively(root_state, state_id, parent_id)



def update_all_same_name_script_recursively(container_state, template_state, ignore_modify):
    template_state_name = template_state.name


    if template_state_name in config.global_config.get_config_value("IGNORED_UPGRADE_STATE_NAME"):
        logger.info("Ignore updating state: {}".format(template_state_name))
        return


    ## update all state's script
    for state in container_state.states.values():
        if isinstance(state, ContainerState):
            update_all_same_name_script_recursively(state, template_state, ignore_modify)
        
        if (not ignore_modify) and state.different_from_lib:
            continue

        ##@TODO in a state machine, some state can be the same id in different container
        template_state_name = template_state.name
        this_state_name = state.name

        if template_state_name == this_state_name:
            logger.info("update {}:{}'s source code".format(state.name, state.unique_id))
            state.script_text = template_state.script_text
        


def update_all_same_name_properties_data_recursively(container_state, 
                                                     template_state, 
                                                     ignore_modify):
    template_state_name = template_state.name

    if template_state_name in config.global_config.get_config_value("IGNORED_UPGRADE_STATE_NAME"):
        logger.info("Ignore updating state: {}".format(template_state_name))
        return

    ## update all state's script
    for state in container_state.states.values():
        if isinstance(state, ContainerState):
            update_all_same_name_properties_data_recursively(state, template_state, ignore_modify)
        
        ##@TODO in a state machine, some state can be the same id in different container
        template_state_name = template_state.name
        this_state_name = state.name
        if (not ignore_modify) and state.different_from_lib:
            continue

        if template_state_name == this_state_name:
            def update_state_properties_data(target_state_dict, template_state_dict):

                ## delete the data not in template_state_dict
                def delete_key_recursively(target_state_dict, template_state_dict, target_state, path_list):
                    for key, value in list(target_state_dict.items()):
                        if key not in template_state_dict:
                            path_list.append(key)
                            target_state.remove_properties_data(path_list)
                            del target_state_dict[key]
                            path_list.pop()
                            continue

                        if isinstance(value, dict):
                            path_list.append(key)
                            delete_key_recursively(value, template_state_dict[key], target_state, path_list)
                            path_list.pop()

                def update_key_recursively(target_state_dict, template_state_dict, target_state, path_list):
                    for key, value in template_state_dict.items():
                        if key not in target_state_dict:
                            target_state.add_properties_data(path_list, value, key)
                            target_state_dict[key] = value
                            continue

                        ## we ignore those keys that already in the target_state_dict with the same value type
                        if not isinstance(value, dict):
                            if type(value) == type(target_state_dict[key]):
                                continue
                            else:
                                target_state.add_properties_data(path_list, value, key)
                                target_state_dict[key] = value
                                continue
                        
                        if isinstance(value, dict) and (not isinstance(target_state_dict[key], dict)):
                            target_state.add_properties_data(path_list, value, key)
                            target_state_dict[key] = value
                            continue

                        if isinstance(value, dict) and isinstance(target_state_dict[key], dict):
                            path_list.append(key)
                            update_key_recursively(target_state_dict[key], value, target_state, path_list)
                            path_list.pop()

                delete_key_recursively(target_state_dict, template_state_dict, state, [])
                update_key_recursively(target_state_dict, template_state_dict, state, [])

                ## we update data_source_translation here
                if template_state.data_source_translation:
                    state._set_data_source_translation(template_state.data_source_translation)

            import copy
            target_state_dict = copy.deepcopy(state.properties_data)
            template_state_dict = copy.deepcopy(template_state.properties_data)
            print("target_state_dict", target_state_dict)
            print("template_state_dict", template_state_dict)
            update_state_properties_data(target_state_dict, template_state_dict)

            logger.info("update {}:{}'s properties_data".format(state.name, state.unique_id))



def update_all_same_name_port_data_recursively(container_state, 
                                               template_state, 
                                               ignore_modify):
    template_state_name = template_state.name

    if template_state_name in config.global_config.get_config_value("IGNORED_UPGRADE_STATE_NAME"):
        logger.info("Ignore updating state: {}".format(template_state_name))
        return
    
    ## update all state's script
    template_state_name = template_state.name
    template_state_id_output_data_ports = template_state.output_data_ports
    template_state_id_input_data_ports = template_state.input_data_ports

    for state in container_state.states.values():
        if isinstance(state, ContainerState):
            update_all_same_name_port_data_recursively(state, 
                                                       template_state, 
                                                       ignore_modify)

        template_state_name_temp = template_state.name
        this_state_name = state.name
        if (not ignore_modify) and state.different_from_lib:
            continue

        if template_state_name_temp == this_state_name:
            def update_state_ports_data(target_state, template_state_id_output_data_ports, template_state_id_input_data_ports):
                ## get all {name: key}
                output_target_state_name_dict = {}
                for data_port_id, value in target_state.output_data_ports.items():
                    output_target_state_name_dict[value.name] = data_port_id

                output_template_state_name_dict = {}
                for data_port_id, value in template_state_id_output_data_ports.items():
                    output_template_state_name_dict[value.name] = data_port_id

                input_target_state_name_dict = {}
                _, input_data_properties_data_names = target_state.check_properties_data_input_sanity()
                for data_port_id, value in target_state.input_data_ports.items():
                    if value.name not in input_data_properties_data_names:
                        input_target_state_name_dict[value.name] = data_port_id
                        

                input_template_state_name_dict = {}
                _, input_data_properties_data_names = template_state.check_properties_data_input_sanity()
                for data_port_id, value in template_state_id_input_data_ports.items():
                    if value.name not in input_data_properties_data_names:
                        input_template_state_name_dict[value.name] = data_port_id


                ## delete name not existed in template_state
                for name, data_port_id in output_target_state_name_dict.items():
                    if name not in output_template_state_name_dict.keys():
                        target_state.remove_output_data_port(data_port_id)
                        continue

                for name, data_port_id in input_target_state_name_dict.items():
                    if name not in input_template_state_name_dict.keys():
                        target_state.remove_input_data_port(data_port_id)
                        continue
                
                ## add and update
                for name, data_port_id in output_template_state_name_dict.items():
                    if name not in output_target_state_name_dict.keys():
                        output_port = template_state_id_output_data_ports[data_port_id]
                        target_state.add_output_data_port(output_port.name, 
                                                          output_port.data_type, 
                                                          output_port.default_value)
                        continue
                    elif template_state_id_output_data_ports[data_port_id].data_type == \
                            target_state.output_data_ports[output_target_state_name_dict[name]].data_type:
                        continue

                    else:
                        target_state.remove_output_data_port(output_target_state_name_dict[name])
                        output_port = template_state_id_output_data_ports[data_port_id]
                        target_state.add_output_data_port(output_port.name, 
                                                          output_port.data_type, 
                                                          output_port.default_value)
                        continue

                for name, data_port_id in input_template_state_name_dict.items():
                    if name not in input_target_state_name_dict.keys():
                        input_port = template_state_id_input_data_ports[data_port_id]
                        target_state.add_input_data_port(input_port.name, 
                                                          input_port.data_type, 
                                                          input_port.default_value)
                        continue
                    elif template_state_id_input_data_ports[data_port_id].data_type == \
                            target_state.input_data_ports[input_target_state_name_dict[name]].data_type:
                        continue

                    else:
                        target_state.remove_input_data_port(output_target_state_name_dict[name])
                        input_port = template_state_id_input_data_ports[data_port_id]
                        target_state.add_input_data_port(input_port.name, 
                                                         input_port.data_type, 
                                                         input_port.default_value)
                        continue
            
            update_state_ports_data(state, template_state_id_output_data_ports, template_state_id_input_data_ports)
            logger.info("update {}:{}'s port_data".format(state.name, state.unique_id))


def update_all_same_name_outcome_recursively(container_state, template_state, ignore_modify):
    template_state_name = template_state.name

    if template_state_name in config.global_config.get_config_value("IGNORED_UPGRADE_STATE_NAME"):
        logger.info("Ignore updating state: {}".format(template_state_name))
        return 
    
    template_state_name = template_state.name
    template_state_outcomes = template_state.outcomes

    ## update all state's script
    for state in container_state.states.values():
        if isinstance(state, ContainerState):
            update_all_same_name_outcome_recursively(state, template_state, ignore_modify)
        
        ##@TODO in a state machine, some state can be the same id in different container
        template_state_name_temp = template_state.name
        this_state_name = state.name
        if (not ignore_modify) and state.different_from_lib:
            continue

        if template_state_name_temp == this_state_name:
            
            def update_state_outcomes(target_state, template_state_outcomes):
                ## get all {name: id}
                outcome_target_state_name_dict = {}
                for outcome_id, value in target_state.outcomes.items():
                    outcome_target_state_name_dict[value.name] = outcome_id

                outcome_template_state_name_dict = {}
                for outcome_id, value in template_state_outcomes.items():
                    outcome_template_state_name_dict[value.name] = outcome_id

                ## delete name not existed in template_state
                for name, outcome_id in outcome_target_state_name_dict.items():
                    if name not in outcome_template_state_name_dict.keys():
                        target_state.remove_outcome(outcome_id)
                        continue

                ## add and update
                for name, outcome_id in outcome_template_state_name_dict.items():
                    if name not in outcome_target_state_name_dict.keys():
                        outcome = template_state_outcomes[outcome_id]
                        target_state.add_outcome(outcome.name, 
                                                 outcome_id)
                        continue
                    elif name == target_state.outcomes[outcome_id].name:
                        continue
                    else:
                        target_state.remove_outcome(outcome_id)
                        outcome = template_state_outcomes[outcome_id]
                        target_state.add_outcome(outcome.name, 
                                                 outcome_id)
                        continue

            update_state_outcomes(state, template_state_outcomes)
            logger.info("update {}:{}'s outcomes".format(state.name, state.unique_id))


def find_target_state_from_container_recursively_by_name(container_state, state_name):
    for state_id, state in container_state.states.items():
        if str(state.name) == str(state_name):
            print("Find state: {} in container_state {}".format(state_name, container_state.name))
            return container_state.states[state_id]

    for state in container_state.states.values():
        if isinstance(state, ContainerState):
            print("child container_state's name: {}".format(state.name))
            target_state =  find_target_state_from_container_recursively_by_name(state, state_name)
            if target_state:
                return target_state
                
    return None


def find_state_by_name_globally(search_start_state, state_name):
    root_state = find_root_state_recursively(search_start_state)
    return find_target_state_from_container_recursively_by_name(root_state, state_name)



def find_the_nearest_state_in_concurrency_state(state):
    if isinstance(state.parent, StateMachine):
        return None
    if isinstance(state.parent, ConcurrencyState):
        return state
    else:
        return find_the_nearest_state_in_concurrency_state(state.parent)


def mark_case_in_concurrency_state(root_state, case_type, case_id_value):
    if not hasattr(root_state, 'case_dict'):
        root_state.case_dict = {} 

    root_state.case_dict[case_type] = case_id_value


def check_case_in_concurrency_state(root_state, case_type):
    if hasattr(root_state, 'case_dict'):
        return True
    else:
        return False

def retrieve_case_id_in_concurrency_state(root_state, case_type):
    return root_state.case_dict[case_type]

def get_xyz_lib_path():
    from rafcon.core.config import global_config
    try:
        lib_path = os.path.expanduser(global_config.get_config_value("LIBRARY_PATHS")["xyz-rafcon-lib"])
    except KeyError:
        logger.warning("The key [xyz-rafcon-lib] is not in config file path LIBRARY_PATHS")
        lib_path = ""

    default_search_path = os.path.expanduser("~/xyz-rafcon-lib")

    if os.path.exists(lib_path):
        load_lib_path = lib_path
    else:
        logger.warning("The lib_path: {} in LIBRARY_PATHS is not existed".format(lib_path))
        if os.path.exists(default_search_path):
            load_lib_path = default_search_path
        else:
            logger.warning("The default_search_path: {} is also not existed".format(default_search_path))
            load_lib_path = None
    
    return load_lib_path


def get_relative_path_of_lib(state_name):
    ##TODO some HACK here 
    lib_path = get_xyz_lib_path()
    cur_path = os.getcwd()
    os.chdir(lib_path)
    result = None

    for path in os.listdir("."):
        if os.path.isdir(path):
            cur_path = os.getcwd()
            os.chdir(path)
            for lib_state_name in os.listdir("."):
                if str(state_name) == lib_state_name:
                    cur_path = os.getcwd()
                    os.chdir(lib_state_name)
                    for final_path in os.listdir("."): 
                        if os.path.isdir(final_path) and final_path.startswith(lib_state_name):
                            cur_path = os.getcwd()
                            os.chdir(final_path)
                            result = os.path.join(os.getcwd(), 'script.py')
                            os.chdir(cur_path)
                    os.chdir(cur_path)

            os.chdir(cur_path)
    
    os.chdir(cur_path)
    if result:
        result = os.path.relpath(result, lib_path)
    return result


## https://stackoverflow.com/questions/1769332/script-to-remove-python-comments-docstrings/2962727#2962727
def remove_comments_and_docstrings(source):
    """
    Returns 'source' minus comments and docstrings.
    """
    io_obj = StringIO(source)
    out = ""
    prev_toktype = tokenize.INDENT
    last_lineno = -1
    last_col = 0
    for tok in tokenize.generate_tokens(io_obj.readline):
        token_type = tok[0]
        token_string = tok[1]
        start_line, start_col = tok[2]
        end_line, end_col = tok[3]
        ltext = tok[4]
        # The following two conditionals preserve indentation.
        # This is necessary because we're not using tokenize.untokenize()
        # (because it spits out code with copious amounts of oddly-placed
        # whitespace).
        if start_line > last_lineno:
            last_col = 0
        if start_col > last_col:
            out += (" " * (start_col - last_col))
        # Remove comments:
        if token_type == tokenize.COMMENT:
            pass
        # This series of conditionals removes docstrings:
        elif token_type == tokenize.STRING:
            if prev_toktype != tokenize.INDENT:
        # This is likely a docstring; double-check we're not inside an operator:
                if prev_toktype != tokenize.NEWLINE:
                    # Note regarding NEWLINE vs NL: The tokenize module
                    # differentiates between newlines that start a new statement
                    # and newlines inside of operators such as parens, brackes,
                    # and curly braces.  Newlines inside of operators are
                    # NEWLINE and newlines that start new code are NL.
                    # Catch whole-module docstrings:
                    if start_col > 0:
                        # Unlabelled indentation means we're inside an operator
                        out += token_string
                    # Note regarding the INDENT token: The tokenize module does
                    # not label indentation inside of an operator (parens,
                    # brackets, and curly braces) as actual indentation.
                    # For example:
                    # def foo():
                    #     "The spaces before this docstring are tokenize.INDENT"
                    #     test = [
                    #         "The spaces before this string do not get a token"
                    #     ]
        else:
            out += token_string
        prev_toktype = token_type
        last_col = end_col
        last_lineno = end_line
    return out

def remove_empty_lines(source):
    return "".join([s for s in source.splitlines(True) if s.strip("\r\n")])

def format_code(text):
    try:
        formatted_code = remove_comments_and_docstrings(text)
    except Exception as e:
        logger.error("Cannot remove comments with error: {}".format(e))
        return ""
    
    try:
        formatted_code, changed = FormatCode(formatted_code)
    except Exception as e:
        logger.error("Cannot format code using yapf with error: {}".format(e))
        return ""

    return remove_empty_lines(formatted_code)


def create_backup_name():
    from datetime import datetime
    now = datetime.now().strftime("%Y-%m-%d-%H:%M:%S")
    return now + "-bk"

def backup_old_state_machine(state_machine_path):
    upgrade_backup_folder = config.global_config.get_config_value("UPGRADE_BACKUP_FOLDER")
    upgrade_backup_folder = LibraryManager._clean_path(upgrade_backup_folder)
    if not os.path.exists(upgrade_backup_folder):
        os.makedirs(upgrade_backup_folder)
    
    dst_path = os.path.join(upgrade_backup_folder, create_backup_name())

    shutil.copytree(state_machine_path, dst_path)
    
