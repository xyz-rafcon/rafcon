#!/usr/bin/env python

# Copyright (C) 2015-2018 DLR
#
# All rights reserved. This program and the accompanying materials are made
# available under the terms of the Eclipse Public License v1.0 which
# accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
#
# Contributors:
# Annika Wollschlaeger <annika.wollschlaeger@dlr.de>
# Benno Voggenreiter <benno.voggenreiter@dlr.de>
# Franz Steinmetz <franz.steinmetz@dlr.de>
# Michael Vilzmann <michael.vilzmann@dlr.de>
# Rico Belder <rico.belder@dlr.de>
# Sebastian Brunner <sebastian.brunner@dlr.de>

"""
.. module: a module to enable state machine execution from the command line
   :synopsis: A module to start arbitrary state machines without the GUI and several configurations options

"""

from future import standard_library
standard_library.install_aliases()
import os
import argparse
from os.path import realpath, dirname, join, exists
import signal
import time
from queue import Empty
import threading
import sys
import logging

import rafcon
from yaml_configuration.config import config_path
import rafcon.utils.filesystem as filesystem

from rafcon.core.config import global_config
import rafcon.core.singleton as core_singletons
from rafcon.core.storage import storage
from rafcon.core.states.state import StateExecutionStatus
from rafcon.core.execution.execution_status import ExecutionStatus, StateMachineExecutionStatus

from rafcon.utils import plugins
from rafcon.utils import resources
from rafcon.utils import log

logger = log.get_logger("rafcon.start.rafcon_launch")


class SingleLauncher(object):
    """This class use to launch rafcon's statemachine easily.
    This state is a wrapper of rafcon_core's start.py, constrains the user can 
    only use one state machine on a process. The only needed input of this class is  
    state_machine_path which is the root folder of a state machine's dumped connecting
    path.

    Args:
        state_machine_path (path): specify directories of state-machines that shall be opened. 
                                The path must contain a statemachine.json file
    """
    def __init__(self, state_machine_path, start_state_path=None, config_path=None):

        self._register_signal_handlers(self._signal_handler)
        logger.info("initialize RAFCON ... ")
        self._setup_environment()
        logger.info("parser state machine path ... ")
        self.state_machine_path = self._parse_state_machine_path(state_machine_path)
        self.start_state_path = start_state_path
        self.config_path = config_path
        self._user_abort = False
        self.state_machine = None
        self._open()

    def _setup_environment(self):
        """Ensures that the environmental variable RAFCON_LIB_PATH is existent
        """
        # The RAFCON_LIB_PATH points to a path with common RAFCON libraries
        # If the env variable is not set, we have to determine it.
        if not os.environ.get('RAFCON_LIB_PATH', None):
            rafcon_library_path = resources.get_data_file_path("rafcon", "libraries")
            if rafcon_library_path:
                os.environ['RAFCON_LIB_PATH'] = rafcon_library_path
            else:
                logger.warning("Could not find root directory of RAFCON libraries. Please specify manually using the "
                            "env var RAFCON_LIB_PATH")

        if not os.environ.get('XYZ_TEMPLATE_PATH', None):
            logger.warning("Could not find $XYZ_TEMPLATE_PATH.")
            if os.environ.get("CODE_BASE", None):
                os.environ['XYZ_TEMPLATE_PATH'] = os.path.join(os.environ["CODE_BASE"], "templates")
        
        # Install dummy _ builtin function in case i18.setup_l10n() is not called
        if sys.version_info >= (3,):
            import builtins as builtins23
        else:
            import __builtin__ as builtins23
        if "_" not in builtins23.__dict__:
            builtins23.__dict__["_"] = lambda s: s


    def _parse_state_machine_path(self, path):
        """Parser for argparse checking pfor a proper state machine path

        :param str path: Input path from the user
        :return: The path
        :raises argparse.ArgumentTypeError: if the path does not contain a statemachine.json file
        """
        sm_root_file = join(path, storage.STATEMACHINE_FILE)
        if exists(sm_root_file):
            return path
        else:
            sm_root_file = join(path, storage.STATEMACHINE_FILE_OLD)
            if exists(sm_root_file):
                return path
            raise argparse.ArgumentTypeError("Failed to open {0}: {1} not found in path".format(path,
                                                                                                storage.STATEMACHINE_FILE))

    def _open(self):
        self.state_machine = storage.load_state_machine_from_path(self.state_machine_path)
        logger.info("add new state machine")
        core_singletons.state_machine_manager.add_state_machine(self.state_machine)
        ## only support one state machine in state machine manager
        assert len(core_singletons.state_machine_manager.state_machines) == 1
    
    def _register_signal_handlers(self, callback):
        for signal_code in [signal.SIGHUP, signal.SIGINT, signal.SIGTERM]:
            signal.signal(signal_code, callback)

    def _signal_handler(self, signal, frame):
        state_machine_execution_engine = core_singletons.state_machine_execution_engine
        core_singletons.shut_down_signal = signal

        logger.info("Shutting down ...")

        try:
            if not state_machine_execution_engine.finished_or_stopped():
                state_machine_execution_engine.stop()
                state_machine_execution_engine.join(3)  # Wait max 3 sec for the execution to stop
        except Exception:
            logger.exception("Could not stop state machine")

        self._user_abort = True

        logging.shutdown()

    def start(self):
        """To start this state machine
        """
        core_singletons.state_machine_execution_engine.start(self.state_machine.state_machine_id, 
                                start_state_path = self.start_state_path)

    def stop(self):
        """Set this state machine's current state stopped, this operation just notify 
        the state machine need to be stopped.
        """
        state_machine_execution_engine = core_singletons.state_machine_execution_engine
        state_machine_execution_engine.stop()

    def pause(self):
        """Set this state machine's current state paused, this operation just notify 
        the state machine need to be paused.
        """
        state_machine_execution_engine = core_singletons.state_machine_execution_engine
        state_machine_execution_engine.pause()
   
    def get_state_machine_operation_state(self):
        """Get this state machine's operation state.
        When running mode:
        the state_machine_operation_state can be 'STARTED',  'STOPPED', 'PAUSED', 'FINISHED'

        When debug mode:
        the state_machine_operation_state can be 'STEP_MODE', 'FORWARD_INTO', 'FORWARD_OVER', 'FORWARD_OUT'
        'BACKWARD', 'RUN_TO_SELECTED_STATE'.
        """
        state_machine_execution_engine = core_singletons.state_machine_execution_engine
        return state_machine_execution_engine.status.execution_mode.name

    def get_state_execution_state(self):
        """Get this state's execution state.
        the state_execution_state can be 'INACTIVE', 'ACTIVE', 'EXECUTE_CHILDREN', 'WAIT_FOR_NEXT_STATE'
        """ 
        return self.state_machine.root_state.state_execution_status.name

    def is_state_running(self):
        """ To Check if the state maching is running. When the state machine is blocked after call stop(), 
        we can still believe in this api.
        """
        return self.state_machine.root_state.state_execution_status is not StateExecutionStatus.INACTIVE

    def wait_for_state_machine_finished(self):
        """Wait this state machine finished
        """

        from rafcon.core.states.execution_state import ExecutionState
        if not isinstance(self.state_machine.root_state, ExecutionState):
            while len(self.state_machine.execution_histories[0]) < 1:
                time.sleep(0.1)
        else:
            time.sleep(0.5)
        
        logger.verbose("Wait for stopped")
        while self.state_machine.root_state.state_execution_status is not StateExecutionStatus.INACTIVE:
            try:
                self.state_machine.root_state.concurrency_queue.get(timeout=1)
                # this check triggers if the state machine could not be stopped in the signal handler
                if self._user_abort:
                    return
            except Empty:
                pass
            # no logger output here to make it easier for the parser
            logger.verbose("RAFCON live signal")

    def close(self):
        """ Close the the state that already opened. You must close the old state machine before open a new one.
        """
        core_singletons.state_machine_manager.delete_all_state_machines()

